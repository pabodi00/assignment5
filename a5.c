#include<stdio.h>

#define PerformanceCost 500
#define PerAttendee 3

//Funtions
int NoOfAttendees(int price);
int Income(int price);
int Cost(int price);
int Profit(int price);


int	NoOfAttendees(int price){
	return 120-(price-15)/5*20;
}

int Income(int price){
	return NoOfAttendees(price)*price;
}

int Cost(int price){
	return NoOfAttendees(price)*PerAttendee+PerformanceCost;
}

int Profit(int price){
	return Income(price)-Cost(price);
}

int main(){
	int price;
	printf("\nExpected Profit for Ticket Prices: \n\n");
	for(price=5;price<50;price+=5)
	{
		printf("Price of ticket = Rs.%d \t\t Profit = Rs.%d\n",price,Profit(price));
		printf("-----------------------------------------------------\n\n");
    }
		return 0;
	}
